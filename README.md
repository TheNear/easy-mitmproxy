# О Mitmproxy

> A collection of some handy [mitmproxy](https://github.com/mitmproxy/mitmproxy) inline scripts.


## Подготовка

1. Install [mitmproxy](https://docs.mitmproxy.org/stable/overview-installation/)

2. [Configure client browser or device](https://docs.mitmproxy.org/stable/overview-getting-started/#configure-your-browser-or-device): configure proxy settings and install CA on client.

### Использование

`./mitm-rewrite.py` can return mock JSON response for certain target URLs.

1. Run `mitmdump`:

```bash
$ mitmdump -s mitm-rewrite.py
```

2. Check `rewrite-router.yaml`, to link response JSON file, for e.g:

```yaml
http://example.com/pass: test_pass
http://example.com/fail: test_fail
```

It means that the response of "http://exmaple.com/pass" will be overwritten by the content in `./response/test_pass.json` file and the response of "http://exmaple.com/fail" will be overwritten by the content in `./response/test_fail.json` file.

3. Edit response JSON file to put mock data you want:

```json
{
  "status": 200,
  "header": { ... },
  "content": ...
}
```

- status: http status code, an INT number
- header: http response headers
- content: response body

The changes in router yaml file and json response files will be applied **on the fly**, no need to restart proxy. Here is an example how it looks like:

![mitm-rewrite-example](screenshot/mitm-rewrite-example.jpg)
