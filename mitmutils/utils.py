import os
import json
from ruamel.yaml import YAML
from time import sleep
import logging


def readFile(file):
    if not os.path.isfile(file):
        logging.error("File: " + file + ' not found!')
        return None

    fname, fext = os.path.splitext(file)

    with open(file) as data:
        if fext == ".yaml":
            yaml = YAML(typ='safe')
            return yaml.load(data)
        else:
            return json.load(data)