from mitmproxy import http
from mitmutils import utils
import logging
import re
import json

HOME_DIR = './'
DATA_DIR = HOME_DIR + 'response/'
ROUTER_FILE = HOME_DIR + 'rewrite-router.yaml'

def response(flow: http.HTTPFlow) -> None:
    routers = utils.readFile(ROUTER_FILE)
    url = flow.request.url

    if routers is not None:
        for patternURL, jsonfilename in routers.items():
            condition_met = (patternURL == url if '://' in patternURL else re.search(patternURL, url) is not None)
            
            if condition_met:
                jsonfile = DATA_DIR + str(jsonfilename) + '.json'
                logging.warn('>>> FOUND "' + url + '". Send response data from "' + jsonfile + '"')

                data = utils.readFile(jsonfile)

                if data is not None:
                    status = int(data['status'])
                    try:
                        content = json.dumps(data['content'])
                    except:
                        content = ''
                    header = data['header']

                    # Получаем значение заголовка Origin из запроса
                    origin_header = flow.request.headers.get("Origin")
                    if origin_header:
                        # Устанавливаем Access-Control-Allow-Origin в значение Origin из запроса
                        header['Access-Control-Allow-Origin'] = origin_header
                        header['Access-Control-Allow-Credentials'] = 'true'
                    else:
                        # Если заголовок Origin отсутствует, можно использовать "*" или указать дефолтный домен
                        header['Access-Control-Allow-Origin'] = "*"

                    header['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
                    header['Access-Control-Allow-Headers'] = 'X-Requested-With, Content-Type'
                    
                    # Обрабатываем предварительные запросы CORS для метода OPTIONS
                    if flow.request.method == "OPTIONS":
                        header['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
                        header['Access-Control-Max-Age'] = '3600'
                        content = ''
                        status = 200

                    flow.response = http.Response.make(status, content, header)
